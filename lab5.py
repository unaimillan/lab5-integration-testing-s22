import random
import asyncio
from itertools import product

import aiohttp

API_KEY = 'AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w'
EMAIL = 'm.kuskov@innopolis.university'
SPEC_URL = f'https://script.google.com/macros/s/{API_KEY}/exec?service=getSpec&email={EMAIL}'

# specs = req.get(SPEC_URL).content.decode()

budget_price_per_min = 0
luxury_price_per_min = 0
price_per_km = 0
deviation = 0
discount = 0


def calc_actual_cost():
    pass


ride_types = ['budget', 'luxury']
ride_plans = ['minute', 'fixed_price']
ride_distances = ['-10', '0', '10']
ride_planned_distances = ['-10', '0', '10']
ride_times = ['-10', '0', '10']
ride_planned_times = ['-10', '0', '10']
ride_discounts = ['yes', '*', 'no']

urls = []
for i, (ride_type, ride_plan, ride_distance, ride_planned_distance, ride_time, ride_planned_time, ride_discount) in enumerate(product(
        ride_types, ride_plans, ride_distances, ride_planned_distances,
        ride_times, ride_planned_times, ride_discounts), 1):
    ride_price_url = f'''\
https://script.google.com/macros/s/{API_KEY}/exec?service=calculatePrice\
&email={EMAIL}&type={ride_type}&plan={ride_plan}&distance={ride_distance}\
&planned_distance={ride_planned_distance}&time={ride_time}\
&planned_time={ride_planned_time}&inno_discount={ride_discount}'''
    urls.append(ride_price_url)
    # resp = req.get(ride_price_url).content.decode()
    print(i, ride_type, ride_plan, ride_distance, ride_planned_distance, ride_time,
          ride_planned_time, ride_discount, sep='|')
    # print(resp)


async def request(sess, i, url):
    async with sess.get(url) as response:
        text = await response.text()
        print(i, text, sep='|')


async def main():
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(limit=10)) as session:
        await asyncio.gather(
            *[asyncio.ensure_future(request(session, i, url)) for i, url in
              enumerate(urls, 1)])


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
