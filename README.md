# Lab5 -- Integration testing

## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you
need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of
module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it
can be used both for whitebox and blackbox testing, depending on your goals and possibilities.   ***Let's roll!***🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used
less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were
doing integration tests, usually it is a task for the automated testing team, to check integration of one module with
another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough,
because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few
modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence
classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.

## Lab

Key for spring semester in 2022 is `AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w`

Ok, finally, we won't need to write any code today :). Hope you're happy)

1. Create your fork of the `
   Lab5 - Integration testing
   ` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab5-integration-testing)
2. Try to use InnoDrive application:

- At first we need to check our default parameters, for it we will need some REST client preferrably(Insomnia, Postman
  or you may use your browser), install it.
- Then let's read through the description of our system:
  The InnoDrive is a hypothetical car sharing service that provides transportations means in Kazan region including
  Innopolis. The service proposes two types of cars `budget` and `luxury`. The service offers flexible two tariff plans.
  With the `minute` plan, the service charges its client by `time per minute of use`. The Service also proposes
  a `fixed_price` plan whereas the price is fixed at the reservation time when the route is chosen. If the driver
  deviates from the planned route for more than `n%`, the tariff plan automatically switches to the `minute` plan. The
  deviation is calculated as difference in the distance or duration from the originally planned route. The `fixed_price`
  plan is available for `budget` cars only. The Innopolis city sponsors the mobility and offers `m%` discount to
  Innopolis residents. Imagine you are the quality engineer of Innodrive. How are you going to know about application
  quality???
- InnoDrive application is just a web application that allows you to compute the price for a certain car ride
- To get particularly your defaults, let's send this request:
  `https://script.google.com/macros/s/_your_key_/exec?service=getSpec&email=_your_email_`
  This will return our default spec(your result might be different):
  Here is InnoCar Specs:

        Budet car price per minute = 17

        Luxury car price per minute = 33

        Fixed price per km = 11

        Allowed deviations in % = 10

        Inno discount in % = 10

- To get the price for certain car ride, let's send next request:
  `https://script.google.com/macros/s/_your_key_/exec?service=calculatePrice&email=_your_email_&type=_type_&plan=_plan_&distance=_distance_&planned_distance=_planned_distance_&time=_time_&planned_time=_planned_time_&inno_discount=_discount_`
  with some random parameters. Answer should be like `{"price": 100}` or `Invalid Request`
- And next we can create our BVA table, this is a lab, so I will create it just for 2 parameters:
  `distance` and `type`.
  `distance` is an integer value, so we can build 2 equivalence classes:

+ `distance` <= 0
+ `distance` > 0 while `type` have 3 equivalence classes:
+ `budget`
+ `luxury`
+ or some `nonsense`.

- Let's use BVA to make integration tests,we need to make few different testcases with `distance`, depending on its
  value:

+ `distance` <= 0 : -10 0
+ `distance` > 0 : 1 100000 This way we will test both our borders and our normal values, we can do the same thing
  for `type`:
+ `type` = "budget"
+ `type` = "luxury"
+ `type` = "Guten morgen sonnenschein"

- Now, let's check responses for different values of our parameters, with all other parameters already setted up, so we
  will have:
    + `plan` = `minute`
    + `planned_distance` = `100`
    + `time` = `110`
    + `planned_time` = `100`
    + `inno_discount` = `yes`
      Let's build test cases out of this data:

| Test case  | distance |   type    | Expected result      |
|------------|----------|-----------|----------------------|
|     1      |    -10   |     *     |   Invalid Request    |
|     2      |    0     |     *     |   Invalid Request    |
|     3      |    *     | "nonsense"|   Invalid Request    |
|     4      |    1     |"budget"   |   1683               |
|     5      |    1     |"luxury"   |   3267               |
|     6      | 1000000  |"budget"   |   1683               |
|     7      | 1000000  |"luxury"   |   3267               |

etc...(there are a lot of combinations), to pick only needed one it is a good practice to use specialized tools. Let's
use Decision table to cut out our tests:

| Conditions(inputs)    | Values                         | R1         | R2        | R3      | R4        |
|-----------------------|--------------------------------|------------|-----------|---------|-----------|
| Type                  | "budget","luxury", nonsense    | nonsense   | budget    | luxury  | *         |
| Distance              | >0, <=0                        | *          | >0        | >0      | <=0       |
| --------------------- | ------------------------------ | ---------- | --------- | ------- | --------- | 
| Invalid Request       |                                | X          |           |         | X         |
| 200                   |                                |            | X         | X       |           |

Now let's use this request to test our values

## Homework

As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug
somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the
Readme of your branch.

## Solution

My particular specs:

```text
Here is InnoCar Specs:
Budet car price per minute = 30
Luxury car price per minute = 65
Fixed price per km = 27
Allowed deviations in % = 7.000000000000001
Inno discount in % = 17
```

### BVA Table

| Parameter        | Equivalence class           |
|------------------|-----------------------------|
| distance         | `<=0`,`>0`                  |
| type             | `budget`,`luxury`,`*`       |
| plan             | `minute`,`fixed_price`, `*` |
| planned_distance | `<=0`,`>0`                  |
| time             | `<=0`,`>0`                  |
| planned_time     | `<=0`,`>0`                  |
| inno_discount    | `yes`,`no`,`*`              |

### Discovered bugs

1. Rides are available with negative distance
2. Rides are available with zero planned distance 
3. Rides are available with zero planned time

### Decision Table

| Test | Type   | Plan        | Distance | Planned distance | Time | Planned time | Discount | Actual price     |
|------|--------|-------------|----------|------------------|------|--------------|----------|------------------|
| 1    | budget | minute      | -10      | -10              | -10  | -10          | yes      | X                |
| 2    | budget | minute      | -10      | -10              | -10  | -10          | *        | X                |
| 3    | budget | minute      | -10      | -10              | -10  | -10          | no       | X                |
| 4    | budget | minute      | -10      | -10              | -10  | 0            | yes      | X                |
| 5    | budget | minute      | -10      | -10              | -10  | 0            | *        | X                |
| 6    | budget | minute      | -10      | -10              | -10  | 0            | no       | X                |
| 7    | budget | minute      | -10      | -10              | -10  | 10           | yes      | X                |
| 8    | budget | minute      | -10      | -10              | -10  | 10           | *        | X                |
| 9    | budget | minute      | -10      | -10              | -10  | 10           | no       | X                |
| 10   | budget | minute      | -10      | -10              | 0    | -10          | yes      | X                |
| 11   | budget | minute      | -10      | -10              | 0    | -10          | *        | X                |
| 12   | budget | minute      | -10      | -10              | 0    | -10          | no       | X                |
| 13   | budget | minute      | -10      | -10              | 0    | 0            | yes      | 0                |
| 14   | budget | minute      | -10      | -10              | 0    | 0            | *        | X                |
| 15   | budget | minute      | -10      | -10              | 0    | 0            | no       | 0                |
| 16   | budget | minute      | -10      | -10              | 0    | 10           | yes      | 0                |
| 17   | budget | minute      | -10      | -10              | 0    | 10           | *        | X                |
| 18   | budget | minute      | -10      | -10              | 0    | 10           | no       | 0                |
| 19   | budget | minute      | -10      | -10              | 10   | -10          | yes      | X                |
| 20   | budget | minute      | -10      | -10              | 10   | -10          | *        | X                |
| 21   | budget | minute      | -10      | -10              | 10   | -10          | no       | X                |
| 22   | budget | minute      | -10      | -10              | 10   | 0            | yes      | 264              |
| 23   | budget | minute      | -10      | -10              | 10   | 0            | *        | X                |
| 24   | budget | minute      | -10      | -10              | 10   | 0            | no       | 300              |
| 25   | budget | minute      | -10      | -10              | 10   | 10           | yes      | 264              |
| 26   | budget | minute      | -10      | -10              | 10   | 10           | *        | X                |
| 27   | budget | minute      | -10      | -10              | 10   | 10           | no       | 300              |
| 28   | budget | minute      | -10      | 0                | -10  | -10          | yes      | X                |
| 29   | budget | minute      | -10      | 0                | -10  | -10          | *        | X                |
| 30   | budget | minute      | -10      | 0                | -10  | -10          | no       | X                |
| 31   | budget | minute      | -10      | 0                | -10  | 0            | yes      | X                |
| 32   | budget | minute      | -10      | 0                | -10  | 0            | *        | X                |
| 33   | budget | minute      | -10      | 0                | -10  | 0            | no       | X                |
| 34   | budget | minute      | -10      | 0                | -10  | 10           | yes      | X                |
| 35   | budget | minute      | -10      | 0                | -10  | 10           | *        | X                |
| 36   | budget | minute      | -10      | 0                | -10  | 10           | no       | X                |
| 37   | budget | minute      | -10      | 0                | 0    | -10          | yes      | X                |
| 38   | budget | minute      | -10      | 0                | 0    | -10          | *        | X                |
| 39   | budget | minute      | -10      | 0                | 0    | -10          | no       | X                |
| 40   | budget | minute      | -10      | 0                | 0    | 0            | yes      | 0                |
| 41   | budget | minute      | -10      | 0                | 0    | 0            | *        | X                |
| 42   | budget | minute      | -10      | 0                | 0    | 0            | no       | 0                |
| 43   | budget | minute      | -10      | 0                | 0    | 10           | yes      | 0                |
| 44   | budget | minute      | -10      | 0                | 0    | 10           | *        | X                |
| 45   | budget | minute      | -10      | 0                | 0    | 10           | no       | 0                |
| 46   | budget | minute      | -10      | 0                | 10   | -10          | yes      | X                |
| 47   | budget | minute      | -10      | 0                | 10   | -10          | *        | X                |
| 48   | budget | minute      | -10      | 0                | 10   | -10          | no       | X                |
| 49   | budget | minute      | -10      | 0                | 10   | 0            | yes      | 264              |
| 50   | budget | minute      | -10      | 0                | 10   | 0            | *        | X                |
| 51   | budget | minute      | -10      | 0                | 10   | 0            | no       | 300              |
| 52   | budget | minute      | -10      | 0                | 10   | 10           | yes      | 264              |
| 53   | budget | minute      | -10      | 0                | 10   | 10           | *        | X                |
| 54   | budget | minute      | -10      | 0                | 10   | 10           | no       | 300              |
| 55   | budget | minute      | -10      | 10               | -10  | -10          | yes      | X                |
| 56   | budget | minute      | -10      | 10               | -10  | -10          | *        | X                |
| 57   | budget | minute      | -10      | 10               | -10  | -10          | no       | X                |
| 58   | budget | minute      | -10      | 10               | -10  | 0            | yes      | X                |
| 59   | budget | minute      | -10      | 10               | -10  | 0            | *        | X                |
| 60   | budget | minute      | -10      | 10               | -10  | 0            | no       | X                |
| 61   | budget | minute      | -10      | 10               | -10  | 10           | yes      | X                |
| 62   | budget | minute      | -10      | 10               | -10  | 10           | *        | X                |
| 63   | budget | minute      | -10      | 10               | -10  | 10           | no       | X                |
| 64   | budget | minute      | -10      | 10               | 0    | -10          | yes      | X                |
| 65   | budget | minute      | -10      | 10               | 0    | -10          | *        | X                |
| 66   | budget | minute      | -10      | 10               | 0    | -10          | no       | X                |
| 67   | budget | minute      | -10      | 10               | 0    | 0            | yes      | 0                |
| 68   | budget | minute      | -10      | 10               | 0    | 0            | *        | X                |
| 69   | budget | minute      | -10      | 10               | 0    | 0            | no       | 0                |
| 70   | budget | minute      | -10      | 10               | 0    | 10           | yes      | 0                |
| 71   | budget | minute      | -10      | 10               | 0    | 10           | *        | X                |
| 72   | budget | minute      | -10      | 10               | 0    | 10           | no       | 0                |
| 73   | budget | minute      | -10      | 10               | 10   | -10          | yes      | X                |
| 74   | budget | minute      | -10      | 10               | 10   | -10          | *        | X                |
| 75   | budget | minute      | -10      | 10               | 10   | -10          | no       | X                |
| 76   | budget | minute      | -10      | 10               | 10   | 0            | yes      | 264              |
| 77   | budget | minute      | -10      | 10               | 10   | 0            | *        | X                |
| 78   | budget | minute      | -10      | 10               | 10   | 0            | no       | 300              |
| 79   | budget | minute      | -10      | 10               | 10   | 10           | yes      | 264              |
| 80   | budget | minute      | -10      | 10               | 10   | 10           | *        | X                |
| 81   | budget | minute      | -10      | 10               | 10   | 10           | no       | 300              |
| 82   | budget | minute      | 0        | -10              | -10  | -10          | yes      | X                |
| 83   | budget | minute      | 0        | -10              | -10  | -10          | *        | X                |
| 84   | budget | minute      | 0        | -10              | -10  | -10          | no       | X                |
| 85   | budget | minute      | 0        | -10              | -10  | 0            | yes      | X                |
| 86   | budget | minute      | 0        | -10              | -10  | 0            | *        | X                |
| 87   | budget | minute      | 0        | -10              | -10  | 0            | no       | X                |
| 88   | budget | minute      | 0        | -10              | -10  | 10           | yes      | X                |
| 89   | budget | minute      | 0        | -10              | -10  | 10           | *        | X                |
| 90   | budget | minute      | 0        | -10              | -10  | 10           | no       | X                |
| 91   | budget | minute      | 0        | -10              | 0    | -10          | yes      | X                |
| 92   | budget | minute      | 0        | -10              | 0    | -10          | *        | X                |
| 93   | budget | minute      | 0        | -10              | 0    | -10          | no       | X                |
| 94   | budget | minute      | 0        | -10              | 0    | 0            | yes      | 0                |
| 95   | budget | minute      | 0        | -10              | 0    | 0            | *        | X                |
| 96   | budget | minute      | 0        | -10              | 0    | 0            | no       | 0                |
| 97   | budget | minute      | 0        | -10              | 0    | 10           | yes      | 0                |
| 98   | budget | minute      | 0        | -10              | 0    | 10           | *        | X                |
| 99   | budget | minute      | 0        | -10              | 0    | 10           | no       | 0                |
| 100  | budget | minute      | 0        | -10              | 10   | -10          | yes      | X                |
| 101  | budget | minute      | 0        | -10              | 10   | -10          | *        | X                |
| 102  | budget | minute      | 0        | -10              | 10   | -10          | no       | X                |
| 103  | budget | minute      | 0        | -10              | 10   | 0            | yes      | 264              |
| 104  | budget | minute      | 0        | -10              | 10   | 0            | *        | X                |
| 105  | budget | minute      | 0        | -10              | 10   | 0            | no       | 300              |
| 106  | budget | minute      | 0        | -10              | 10   | 10           | yes      | 264              |
| 107  | budget | minute      | 0        | -10              | 10   | 10           | *        | X                |
| 108  | budget | minute      | 0        | -10              | 10   | 10           | no       | 300              |
| 109  | budget | minute      | 0        | 0                | -10  | -10          | yes      | X                |
| 110  | budget | minute      | 0        | 0                | -10  | -10          | *        | X                |
| 111  | budget | minute      | 0        | 0                | -10  | -10          | no       | X                |
| 112  | budget | minute      | 0        | 0                | -10  | 0            | yes      | X                |
| 113  | budget | minute      | 0        | 0                | -10  | 0            | *        | X                |
| 114  | budget | minute      | 0        | 0                | -10  | 0            | no       | X                |
| 115  | budget | minute      | 0        | 0                | -10  | 10           | yes      | X                |
| 116  | budget | minute      | 0        | 0                | -10  | 10           | *        | X                |
| 117  | budget | minute      | 0        | 0                | -10  | 10           | no       | X                |
| 118  | budget | minute      | 0        | 0                | 0    | -10          | yes      | X                |
| 119  | budget | minute      | 0        | 0                | 0    | -10          | *        | X                |
| 120  | budget | minute      | 0        | 0                | 0    | -10          | no       | X                |
| 121  | budget | minute      | 0        | 0                | 0    | 0            | yes      | 0                |
| 122  | budget | minute      | 0        | 0                | 0    | 0            | *        | X                |
| 123  | budget | minute      | 0        | 0                | 0    | 0            | no       | 0                |
| 124  | budget | minute      | 0        | 0                | 0    | 10           | yes      | 0                |
| 125  | budget | minute      | 0        | 0                | 0    | 10           | *        | X                |
| 126  | budget | minute      | 0        | 0                | 0    | 10           | no       | 0                |
| 127  | budget | minute      | 0        | 0                | 10   | -10          | yes      | X                |
| 128  | budget | minute      | 0        | 0                | 10   | -10          | *        | X                |
| 129  | budget | minute      | 0        | 0                | 10   | -10          | no       | X                |
| 130  | budget | minute      | 0        | 0                | 10   | 0            | yes      | 264              |
| 131  | budget | minute      | 0        | 0                | 10   | 0            | *        | X                |
| 132  | budget | minute      | 0        | 0                | 10   | 0            | no       | 300              |
| 133  | budget | minute      | 0        | 0                | 10   | 10           | yes      | 264              |
| 134  | budget | minute      | 0        | 0                | 10   | 10           | *        | X                |
| 135  | budget | minute      | 0        | 0                | 10   | 10           | no       | 300              |
| 136  | budget | minute      | 0        | 10               | -10  | -10          | yes      | X                |
| 137  | budget | minute      | 0        | 10               | -10  | -10          | *        | X                |
| 138  | budget | minute      | 0        | 10               | -10  | -10          | no       | X                |
| 139  | budget | minute      | 0        | 10               | -10  | 0            | yes      | X                |
| 140  | budget | minute      | 0        | 10               | -10  | 0            | *        | X                |
| 141  | budget | minute      | 0        | 10               | -10  | 0            | no       | X                |
| 142  | budget | minute      | 0        | 10               | -10  | 10           | yes      | X                |
| 143  | budget | minute      | 0        | 10               | -10  | 10           | *        | X                |
| 144  | budget | minute      | 0        | 10               | -10  | 10           | no       | X                |
| 145  | budget | minute      | 0        | 10               | 0    | -10          | yes      | X                |
| 146  | budget | minute      | 0        | 10               | 0    | -10          | *        | X                |
| 147  | budget | minute      | 0        | 10               | 0    | -10          | no       | X                |
| 148  | budget | minute      | 0        | 10               | 0    | 0            | yes      | 0                |
| 149  | budget | minute      | 0        | 10               | 0    | 0            | *        | X                |
| 150  | budget | minute      | 0        | 10               | 0    | 0            | no       | 0                |
| 151  | budget | minute      | 0        | 10               | 0    | 10           | yes      | 0                |
| 152  | budget | minute      | 0        | 10               | 0    | 10           | *        | X                |
| 153  | budget | minute      | 0        | 10               | 0    | 10           | no       | 0                |
| 154  | budget | minute      | 0        | 10               | 10   | -10          | yes      | X                |
| 155  | budget | minute      | 0        | 10               | 10   | -10          | *        | X                |
| 156  | budget | minute      | 0        | 10               | 10   | -10          | no       | X                |
| 157  | budget | minute      | 0        | 10               | 10   | 0            | yes      | 264              |
| 158  | budget | minute      | 0        | 10               | 10   | 0            | *        | X                |
| 159  | budget | minute      | 0        | 10               | 10   | 0            | no       | 300              |
| 160  | budget | minute      | 0        | 10               | 10   | 10           | yes      | 264              |
| 161  | budget | minute      | 0        | 10               | 10   | 10           | *        | X                |
| 162  | budget | minute      | 0        | 10               | 10   | 10           | no       | 300              |
| 163  | budget | minute      | 10       | -10              | -10  | -10          | yes      | X                |
| 164  | budget | minute      | 10       | -10              | -10  | -10          | *        | X                |
| 165  | budget | minute      | 10       | -10              | -10  | -10          | no       | X                |
| 166  | budget | minute      | 10       | -10              | -10  | 0            | yes      | X                |
| 167  | budget | minute      | 10       | -10              | -10  | 0            | *        | X                |
| 168  | budget | minute      | 10       | -10              | -10  | 0            | no       | X                |
| 169  | budget | minute      | 10       | -10              | -10  | 10           | yes      | X                |
| 170  | budget | minute      | 10       | -10              | -10  | 10           | *        | X                |
| 171  | budget | minute      | 10       | -10              | -10  | 10           | no       | X                |
| 172  | budget | minute      | 10       | -10              | 0    | -10          | yes      | X                |
| 173  | budget | minute      | 10       | -10              | 0    | -10          | *        | X                |
| 174  | budget | minute      | 10       | -10              | 0    | -10          | no       | X                |
| 175  | budget | minute      | 10       | -10              | 0    | 0            | yes      | 0                |
| 176  | budget | minute      | 10       | -10              | 0    | 0            | *        | X                |
| 177  | budget | minute      | 10       | -10              | 0    | 0            | no       | 0                |
| 178  | budget | minute      | 10       | -10              | 0    | 10           | yes      | 0                |
| 179  | budget | minute      | 10       | -10              | 0    | 10           | *        | X                |
| 180  | budget | minute      | 10       | -10              | 0    | 10           | no       | 0                |
| 181  | budget | minute      | 10       | -10              | 10   | -10          | yes      | X                |
| 182  | budget | minute      | 10       | -10              | 10   | -10          | *        | X                |
| 183  | budget | minute      | 10       | -10              | 10   | -10          | no       | X                |
| 184  | budget | minute      | 10       | -10              | 10   | 0            | yes      | 264              |
| 185  | budget | minute      | 10       | -10              | 10   | 0            | *        | X                |
| 186  | budget | minute      | 10       | -10              | 10   | 0            | no       | 300              |
| 187  | budget | minute      | 10       | -10              | 10   | 10           | yes      | 264              |
| 188  | budget | minute      | 10       | -10              | 10   | 10           | *        | X                |
| 189  | budget | minute      | 10       | -10              | 10   | 10           | no       | 300              |
| 190  | budget | minute      | 10       | 0                | -10  | -10          | yes      | X                |
| 191  | budget | minute      | 10       | 0                | -10  | -10          | *        | X                |
| 192  | budget | minute      | 10       | 0                | -10  | -10          | no       | X                |
| 193  | budget | minute      | 10       | 0                | -10  | 0            | yes      | X                |
| 194  | budget | minute      | 10       | 0                | -10  | 0            | *        | X                |
| 195  | budget | minute      | 10       | 0                | -10  | 0            | no       | X                |
| 196  | budget | minute      | 10       | 0                | -10  | 10           | yes      | X                |
| 197  | budget | minute      | 10       | 0                | -10  | 10           | *        | X                |
| 198  | budget | minute      | 10       | 0                | -10  | 10           | no       | X                |
| 199  | budget | minute      | 10       | 0                | 0    | -10          | yes      | X                |
| 200  | budget | minute      | 10       | 0                | 0    | -10          | *        | X                |
| 201  | budget | minute      | 10       | 0                | 0    | -10          | no       | X                |
| 202  | budget | minute      | 10       | 0                | 0    | 0            | yes      | 0                |
| 203  | budget | minute      | 10       | 0                | 0    | 0            | *        | X                |
| 204  | budget | minute      | 10       | 0                | 0    | 0            | no       | 0                |
| 205  | budget | minute      | 10       | 0                | 0    | 10           | yes      | 0                |
| 206  | budget | minute      | 10       | 0                | 0    | 10           | *        | X                |
| 207  | budget | minute      | 10       | 0                | 0    | 10           | no       | 0                |
| 208  | budget | minute      | 10       | 0                | 10   | -10          | yes      | X                |
| 209  | budget | minute      | 10       | 0                | 10   | -10          | *        | X                |
| 210  | budget | minute      | 10       | 0                | 10   | -10          | no       | X                |
| 211  | budget | minute      | 10       | 0                | 10   | 0            | yes      | 264              |
| 212  | budget | minute      | 10       | 0                | 10   | 0            | *        | X                |
| 213  | budget | minute      | 10       | 0                | 10   | 0            | no       | 300              |
| 214  | budget | minute      | 10       | 0                | 10   | 10           | yes      | 264              |
| 215  | budget | minute      | 10       | 0                | 10   | 10           | *        | X                |
| 216  | budget | minute      | 10       | 0                | 10   | 10           | no       | 300              |
| 217  | budget | minute      | 10       | 10               | -10  | -10          | yes      | X                |
| 218  | budget | minute      | 10       | 10               | -10  | -10          | *        | X                |
| 219  | budget | minute      | 10       | 10               | -10  | -10          | no       | X                |
| 220  | budget | minute      | 10       | 10               | -10  | 0            | yes      | X                |
| 221  | budget | minute      | 10       | 10               | -10  | 0            | *        | X                |
| 222  | budget | minute      | 10       | 10               | -10  | 0            | no       | X                |
| 223  | budget | minute      | 10       | 10               | -10  | 10           | yes      | X                |
| 224  | budget | minute      | 10       | 10               | -10  | 10           | *        | X                |
| 225  | budget | minute      | 10       | 10               | -10  | 10           | no       | X                |
| 226  | budget | minute      | 10       | 10               | 0    | -10          | yes      | X                |
| 227  | budget | minute      | 10       | 10               | 0    | -10          | *        | X                |
| 228  | budget | minute      | 10       | 10               | 0    | -10          | no       | X                |
| 229  | budget | minute      | 10       | 10               | 0    | 0            | yes      | 0                |
| 230  | budget | minute      | 10       | 10               | 0    | 0            | *        | X                |
| 231  | budget | minute      | 10       | 10               | 0    | 0            | no       | 0                |
| 232  | budget | minute      | 10       | 10               | 0    | 10           | yes      | 0                |
| 233  | budget | minute      | 10       | 10               | 0    | 10           | *        | X                |
| 234  | budget | minute      | 10       | 10               | 0    | 10           | no       | 0                |
| 235  | budget | minute      | 10       | 10               | 10   | -10          | yes      | X                |
| 236  | budget | minute      | 10       | 10               | 10   | -10          | *        | X                |
| 237  | budget | minute      | 10       | 10               | 10   | -10          | no       | X                |
| 238  | budget | minute      | 10       | 10               | 10   | 0            | yes      | 264              |
| 239  | budget | minute      | 10       | 10               | 10   | 0            | *        | X                |
| 240  | budget | minute      | 10       | 10               | 10   | 0            | no       | 300              |
| 241  | budget | minute      | 10       | 10               | 10   | 10           | yes      | 264              |
| 242  | budget | minute      | 10       | 10               | 10   | 10           | *        | X                |
| 243  | budget | minute      | 10       | 10               | 10   | 10           | no       | 300              |
| 244  | budget | fixed_price | -10      | -10              | -10  | -10          | yes      | X                |
| 245  | budget | fixed_price | -10      | -10              | -10  | -10          | *        | X                |
| 246  | budget | fixed_price | -10      | -10              | -10  | -10          | no       | X                |
| 247  | budget | fixed_price | -10      | -10              | -10  | 0            | yes      | X                |
| 248  | budget | fixed_price | -10      | -10              | -10  | 0            | *        | X                |
| 249  | budget | fixed_price | -10      | -10              | -10  | 0            | no       | X                |
| 250  | budget | fixed_price | -10      | -10              | -10  | 10           | yes      | X                |
| 251  | budget | fixed_price | -10      | -10              | -10  | 10           | *        | X                |
| 252  | budget | fixed_price | -10      | -10              | -10  | 10           | no       | X                |
| 253  | budget | fixed_price | -10      | -10              | 0    | -10          | yes      | X                |
| 254  | budget | fixed_price | -10      | -10              | 0    | -10          | *        | X                |
| 255  | budget | fixed_price | -10      | -10              | 0    | -10          | no       | X                |
| 256  | budget | fixed_price | -10      | -10              | 0    | 0            | yes      | 0                |
| 257  | budget | fixed_price | -10      | -10              | 0    | 0            | *        | X                |
| 258  | budget | fixed_price | -10      | -10              | 0    | 0            | no       | 0                |
| 259  | budget | fixed_price | -10      | -10              | 0    | 10           | yes      | 0                |
| 260  | budget | fixed_price | -10      | -10              | 0    | 10           | *        | X                |
| 261  | budget | fixed_price | -10      | -10              | 0    | 10           | no       | 0                |
| 262  | budget | fixed_price | -10      | -10              | 10   | -10          | yes      | X                |
| 263  | budget | fixed_price | -10      | -10              | 10   | -10          | *        | X                |
| 264  | budget | fixed_price | -10      | -10              | 10   | -10          | no       | X                |
| 265  | budget | fixed_price | -10      | -10              | 10   | 0            | yes      | 146.666666666667 |
| 266  | budget | fixed_price | -10      | -10              | 10   | 0            | *        | X                |
| 267  | budget | fixed_price | -10      | -10              | 10   | 0            | no       | 166.666666666667 |
| 268  | budget | fixed_price | -10      | -10              | 10   | 10           | yes      | X                |
| 269  | budget | fixed_price | -10      | -10              | 10   | 10           | *        | X                |
| 270  | budget | fixed_price | -10      | -10              | 10   | 10           | no       | X                |
| 271  | budget | fixed_price | -10      | 0                | -10  | -10          | yes      | X                |
| 272  | budget | fixed_price | -10      | 0                | -10  | -10          | *        | X                |
| 273  | budget | fixed_price | -10      | 0                | -10  | -10          | no       | X                |
| 274  | budget | fixed_price | -10      | 0                | -10  | 0            | yes      | X                |
| 275  | budget | fixed_price | -10      | 0                | -10  | 0            | *        | X                |
| 276  | budget | fixed_price | -10      | 0                | -10  | 0            | no       | X                |
| 277  | budget | fixed_price | -10      | 0                | -10  | 10           | yes      | X                |
| 278  | budget | fixed_price | -10      | 0                | -10  | 10           | *        | X                |
| 279  | budget | fixed_price | -10      | 0                | -10  | 10           | no       | X                |
| 280  | budget | fixed_price | -10      | 0                | 0    | -10          | yes      | X                |
| 281  | budget | fixed_price | -10      | 0                | 0    | -10          | *        | X                |
| 282  | budget | fixed_price | -10      | 0                | 0    | -10          | no       | X                |
| 283  | budget | fixed_price | -10      | 0                | 0    | 0            | yes      | 0                |
| 284  | budget | fixed_price | -10      | 0                | 0    | 0            | *        | X                |
| 285  | budget | fixed_price | -10      | 0                | 0    | 0            | no       | 0                |
| 286  | budget | fixed_price | -10      | 0                | 0    | 10           | yes      | 0                |
| 287  | budget | fixed_price | -10      | 0                | 0    | 10           | *        | X                |
| 288  | budget | fixed_price | -10      | 0                | 0    | 10           | no       | 0                |
| 289  | budget | fixed_price | -10      | 0                | 10   | -10          | yes      | X                |
| 290  | budget | fixed_price | -10      | 0                | 10   | -10          | *        | X                |
| 291  | budget | fixed_price | -10      | 0                | 10   | -10          | no       | X                |
| 292  | budget | fixed_price | -10      | 0                | 10   | 0            | yes      | 146.666666666667 |
| 293  | budget | fixed_price | -10      | 0                | 10   | 0            | *        | X                |
| 294  | budget | fixed_price | -10      | 0                | 10   | 0            | no       | 166.666666666667 |
| 295  | budget | fixed_price | -10      | 0                | 10   | 10           | yes      | 146.666666666667 |
| 296  | budget | fixed_price | -10      | 0                | 10   | 10           | *        | X                |
| 297  | budget | fixed_price | -10      | 0                | 10   | 10           | no       | 166.666666666667 |
| 298  | budget | fixed_price | -10      | 10               | -10  | -10          | yes      | X                |
| 299  | budget | fixed_price | -10      | 10               | -10  | -10          | *        | X                |
| 300  | budget | fixed_price | -10      | 10               | -10  | -10          | no       | X                |
| 301  | budget | fixed_price | -10      | 10               | -10  | 0            | yes      | X                |
| 302  | budget | fixed_price | -10      | 10               | -10  | 0            | *        | X                |
| 303  | budget | fixed_price | -10      | 10               | -10  | 0            | no       | X                |
| 304  | budget | fixed_price | -10      | 10               | -10  | 10           | yes      | X                |
| 305  | budget | fixed_price | -10      | 10               | -10  | 10           | *        | X                |
| 306  | budget | fixed_price | -10      | 10               | -10  | 10           | no       | X                |
| 307  | budget | fixed_price | -10      | 10               | 0    | -10          | yes      | X                |
| 308  | budget | fixed_price | -10      | 10               | 0    | -10          | *        | X                |
| 309  | budget | fixed_price | -10      | 10               | 0    | -10          | no       | X                |
| 310  | budget | fixed_price | -10      | 10               | 0    | 0            | yes      | 0                |
| 311  | budget | fixed_price | -10      | 10               | 0    | 0            | *        | X                |
| 312  | budget | fixed_price | -10      | 10               | 0    | 0            | no       | 0                |
| 313  | budget | fixed_price | -10      | 10               | 0    | 10           | yes      | 0                |
| 314  | budget | fixed_price | -10      | 10               | 0    | 10           | *        | X                |
| 315  | budget | fixed_price | -10      | 10               | 0    | 10           | no       | 0                |
| 316  | budget | fixed_price | -10      | 10               | 10   | -10          | yes      | X                |
| 317  | budget | fixed_price | -10      | 10               | 10   | -10          | *        | X                |
| 318  | budget | fixed_price | -10      | 10               | 10   | -10          | no       | X                |
| 319  | budget | fixed_price | -10      | 10               | 10   | 0            | yes      | 146.666666666667 |
| 320  | budget | fixed_price | -10      | 10               | 10   | 0            | *        | X                |
| 321  | budget | fixed_price | -10      | 10               | 10   | 0            | no       | 166.666666666667 |
| 322  | budget | fixed_price | -10      | 10               | 10   | 10           | yes      | 146.666666666667 |
| 323  | budget | fixed_price | -10      | 10               | 10   | 10           | *        | X                |
| 324  | budget | fixed_price | -10      | 10               | 10   | 10           | no       | 166.666666666667 |
| 325  | budget | fixed_price | 0        | -10              | -10  | -10          | yes      | X                |
| 326  | budget | fixed_price | 0        | -10              | -10  | -10          | *        | X                |
| 327  | budget | fixed_price | 0        | -10              | -10  | -10          | no       | X                |
| 328  | budget | fixed_price | 0        | -10              | -10  | 0            | yes      | X                |
| 329  | budget | fixed_price | 0        | -10              | -10  | 0            | *        | X                |
| 330  | budget | fixed_price | 0        | -10              | -10  | 0            | no       | X                |
| 331  | budget | fixed_price | 0        | -10              | -10  | 10           | yes      | X                |
| 332  | budget | fixed_price | 0        | -10              | -10  | 10           | *        | X                |
| 333  | budget | fixed_price | 0        | -10              | -10  | 10           | no       | X                |
| 334  | budget | fixed_price | 0        | -10              | 0    | -10          | yes      | X                |
| 335  | budget | fixed_price | 0        | -10              | 0    | -10          | *        | X                |
| 336  | budget | fixed_price | 0        | -10              | 0    | -10          | no       | X                |
| 337  | budget | fixed_price | 0        | -10              | 0    | 0            | yes      | 0                |
| 338  | budget | fixed_price | 0        | -10              | 0    | 0            | *        | X                |
| 339  | budget | fixed_price | 0        | -10              | 0    | 0            | no       | 0                |
| 340  | budget | fixed_price | 0        | -10              | 0    | 10           | yes      | 0                |
| 341  | budget | fixed_price | 0        | -10              | 0    | 10           | *        | X                |
| 342  | budget | fixed_price | 0        | -10              | 0    | 10           | no       | 0                |
| 343  | budget | fixed_price | 0        | -10              | 10   | -10          | yes      | X                |
| 344  | budget | fixed_price | 0        | -10              | 10   | -10          | *        | X                |
| 345  | budget | fixed_price | 0        | -10              | 10   | -10          | no       | X                |
| 346  | budget | fixed_price | 0        | -10              | 10   | 0            | yes      | 146.666666666667 |
| 347  | budget | fixed_price | 0        | -10              | 10   | 0            | *        | X                |
| 348  | budget | fixed_price | 0        | -10              | 10   | 0            | no       | 166.666666666667 |
| 349  | budget | fixed_price | 0        | -10              | 10   | 10           | yes      | 146.666666666667 |
| 350  | budget | fixed_price | 0        | -10              | 10   | 10           | *        | X                |
| 351  | budget | fixed_price | 0        | -10              | 10   | 10           | no       | 166.666666666667 |
| 352  | budget | fixed_price | 0        | 0                | -10  | -10          | yes      | X                |
| 353  | budget | fixed_price | 0        | 0                | -10  | -10          | *        | X                |
| 354  | budget | fixed_price | 0        | 0                | -10  | -10          | no       | X                |
| 355  | budget | fixed_price | 0        | 0                | -10  | 0            | yes      | X                |
| 356  | budget | fixed_price | 0        | 0                | -10  | 0            | *        | X                |
| 357  | budget | fixed_price | 0        | 0                | -10  | 0            | no       | X                |
| 358  | budget | fixed_price | 0        | 0                | -10  | 10           | yes      | X                |
| 359  | budget | fixed_price | 0        | 0                | -10  | 10           | *        | X                |
| 360  | budget | fixed_price | 0        | 0                | -10  | 10           | no       | X                |
| 361  | budget | fixed_price | 0        | 0                | 0    | -10          | yes      | X                |
| 362  | budget | fixed_price | 0        | 0                | 0    | -10          | *        | X                |
| 363  | budget | fixed_price | 0        | 0                | 0    | -10          | no       | X                |
| 364  | budget | fixed_price | 0        | 0                | 0    | 0            | yes      | 0                |
| 365  | budget | fixed_price | 0        | 0                | 0    | 0            | *        | X                |
| 366  | budget | fixed_price | 0        | 0                | 0    | 0            | no       | 0                |
| 367  | budget | fixed_price | 0        | 0                | 0    | 10           | yes      | 0                |
| 368  | budget | fixed_price | 0        | 0                | 0    | 10           | *        | X                |
| 369  | budget | fixed_price | 0        | 0                | 0    | 10           | no       | 0                |
| 370  | budget | fixed_price | 0        | 0                | 10   | -10          | yes      | X                |
| 371  | budget | fixed_price | 0        | 0                | 10   | -10          | *        | X                |
| 372  | budget | fixed_price | 0        | 0                | 10   | -10          | no       | X                |
| 373  | budget | fixed_price | 0        | 0                | 10   | 0            | yes      | 146.666666666667 |
| 374  | budget | fixed_price | 0        | 0                | 10   | 0            | *        | X                |
| 375  | budget | fixed_price | 0        | 0                | 10   | 0            | no       | 166.666666666667 |
| 376  | budget | fixed_price | 0        | 0                | 10   | 10           | yes      | 146.666666666667 |
| 377  | budget | fixed_price | 0        | 0                | 10   | 10           | *        | X                |
| 378  | budget | fixed_price | 0        | 0                | 10   | 10           | no       | 166.666666666667 |
| 379  | budget | fixed_price | 0        | 10               | -10  | -10          | yes      | X                |
| 380  | budget | fixed_price | 0        | 10               | -10  | -10          | *        | X                |
| 381  | budget | fixed_price | 0        | 10               | -10  | -10          | no       | X                |
| 382  | budget | fixed_price | 0        | 10               | -10  | 0            | yes      | X                |
| 383  | budget | fixed_price | 0        | 10               | -10  | 0            | *        | X                |
| 384  | budget | fixed_price | 0        | 10               | -10  | 0            | no       | X                |
| 385  | budget | fixed_price | 0        | 10               | -10  | 10           | yes      | X                |
| 386  | budget | fixed_price | 0        | 10               | -10  | 10           | *        | X                |
| 387  | budget | fixed_price | 0        | 10               | -10  | 10           | no       | X                |
| 388  | budget | fixed_price | 0        | 10               | 0    | -10          | yes      | X                |
| 389  | budget | fixed_price | 0        | 10               | 0    | -10          | *        | X                |
| 390  | budget | fixed_price | 0        | 10               | 0    | -10          | no       | X                |
| 391  | budget | fixed_price | 0        | 10               | 0    | 0            | yes      | 0                |
| 392  | budget | fixed_price | 0        | 10               | 0    | 0            | *        | X                |
| 393  | budget | fixed_price | 0        | 10               | 0    | 0            | no       | 0                |
| 394  | budget | fixed_price | 0        | 10               | 0    | 10           | yes      | 0                |
| 395  | budget | fixed_price | 0        | 10               | 0    | 10           | *        | X                |
| 396  | budget | fixed_price | 0        | 10               | 0    | 10           | no       | 0                |
| 397  | budget | fixed_price | 0        | 10               | 10   | -10          | yes      | X                |
| 398  | budget | fixed_price | 0        | 10               | 10   | -10          | *        | X                |
| 399  | budget | fixed_price | 0        | 10               | 10   | -10          | no       | X                |
| 400  | budget | fixed_price | 0        | 10               | 10   | 0            | yes      | 146.666666666667 |
| 401  | budget | fixed_price | 0        | 10               | 10   | 0            | *        | X                |
| 402  | budget | fixed_price | 0        | 10               | 10   | 0            | no       | 166.666666666667 |
| 403  | budget | fixed_price | 0        | 10               | 10   | 10           | yes      | 146.666666666667 |
| 404  | budget | fixed_price | 0        | 10               | 10   | 10           | *        | X                |
| 405  | budget | fixed_price | 0        | 10               | 10   | 10           | no       | 166.666666666667 |
| 406  | budget | fixed_price | 10       | -10              | -10  | -10          | yes      | X                |
| 407  | budget | fixed_price | 10       | -10              | -10  | -10          | *        | X                |
| 408  | budget | fixed_price | 10       | -10              | -10  | -10          | no       | X                |
| 409  | budget | fixed_price | 10       | -10              | -10  | 0            | yes      | X                |
| 410  | budget | fixed_price | 10       | -10              | -10  | 0            | *        | X                |
| 411  | budget | fixed_price | 10       | -10              | -10  | 0            | no       | X                |
| 412  | budget | fixed_price | 10       | -10              | -10  | 10           | yes      | X                |
| 413  | budget | fixed_price | 10       | -10              | -10  | 10           | *        | X                |
| 414  | budget | fixed_price | 10       | -10              | -10  | 10           | no       | X                |
| 415  | budget | fixed_price | 10       | -10              | 0    | -10          | yes      | X                |
| 416  | budget | fixed_price | 10       | -10              | 0    | -10          | *        | X                |
| 417  | budget | fixed_price | 10       | -10              | 0    | -10          | no       | X                |
| 418  | budget | fixed_price | 10       | -10              | 0    | 0            | yes      | 0                |
| 419  | budget | fixed_price | 10       | -10              | 0    | 0            | *        | X                |
| 420  | budget | fixed_price | 10       | -10              | 0    | 0            | no       | 0                |
| 421  | budget | fixed_price | 10       | -10              | 0    | 10           | yes      | 0                |
| 422  | budget | fixed_price | 10       | -10              | 0    | 10           | *        | X                |
| 423  | budget | fixed_price | 10       | -10              | 0    | 10           | no       | 0                |
| 424  | budget | fixed_price | 10       | -10              | 10   | -10          | yes      | X                |
| 425  | budget | fixed_price | 10       | -10              | 10   | -10          | *        | X                |
| 426  | budget | fixed_price | 10       | -10              | 10   | -10          | no       | X                |
| 427  | budget | fixed_price | 10       | -10              | 10   | 0            | yes      | 146.666666666667 |
| 428  | budget | fixed_price | 10       | -10              | 10   | 0            | *        | X                |
| 429  | budget | fixed_price | 10       | -10              | 10   | 0            | no       | 166.666666666667 |
| 430  | budget | fixed_price | 10       | -10              | 10   | 10           | yes      | 146.666666666667 |
| 431  | budget | fixed_price | 10       | -10              | 10   | 10           | *        | X                |
| 432  | budget | fixed_price | 10       | -10              | 10   | 10           | no       | 166.666666666667 |
| 433  | budget | fixed_price | 10       | 0                | -10  | -10          | yes      | X                |
| 434  | budget | fixed_price | 10       | 0                | -10  | -10          | *        | X                |
| 435  | budget | fixed_price | 10       | 0                | -10  | -10          | no       | X                |
| 436  | budget | fixed_price | 10       | 0                | -10  | 0            | yes      | X                |
| 437  | budget | fixed_price | 10       | 0                | -10  | 0            | *        | X                |
| 438  | budget | fixed_price | 10       | 0                | -10  | 0            | no       | X                |
| 439  | budget | fixed_price | 10       | 0                | -10  | 10           | yes      | X                |
| 440  | budget | fixed_price | 10       | 0                | -10  | 10           | *        | X                |
| 441  | budget | fixed_price | 10       | 0                | -10  | 10           | no       | X                |
| 442  | budget | fixed_price | 10       | 0                | 0    | -10          | yes      | X                |
| 443  | budget | fixed_price | 10       | 0                | 0    | -10          | *        | X                |
| 444  | budget | fixed_price | 10       | 0                | 0    | -10          | no       | X                |
| 445  | budget | fixed_price | 10       | 0                | 0    | 0            | yes      | 0                |
| 446  | budget | fixed_price | 10       | 0                | 0    | 0            | *        | X                |
| 447  | budget | fixed_price | 10       | 0                | 0    | 0            | no       | 0                |
| 448  | budget | fixed_price | 10       | 0                | 0    | 10           | yes      | 0                |
| 449  | budget | fixed_price | 10       | 0                | 0    | 10           | *        | X                |
| 450  | budget | fixed_price | 10       | 0                | 0    | 10           | no       | 0                |
| 451  | budget | fixed_price | 10       | 0                | 10   | -10          | yes      | X                |
| 452  | budget | fixed_price | 10       | 0                | 10   | -10          | *        | X                |
| 453  | budget | fixed_price | 10       | 0                | 10   | -10          | no       | X                |
| 454  | budget | fixed_price | 10       | 0                | 10   | 0            | yes      | 146.666666666667 |
| 455  | budget | fixed_price | 10       | 0                | 10   | 0            | *        | X                |
| 456  | budget | fixed_price | 10       | 0                | 10   | 0            | no       | 166.666666666667 |
| 457  | budget | fixed_price | 10       | 0                | 10   | 10           | yes      | 146.666666666667 |
| 458  | budget | fixed_price | 10       | 0                | 10   | 10           | *        | X                |
| 459  | budget | fixed_price | 10       | 0                | 10   | 10           | no       | 166.666666666667 |
| 460  | budget | fixed_price | 10       | 10               | -10  | -10          | yes      | X                |
| 461  | budget | fixed_price | 10       | 10               | -10  | -10          | *        | X                |
| 462  | budget | fixed_price | 10       | 10               | -10  | -10          | no       | X                |
| 463  | budget | fixed_price | 10       | 10               | -10  | 0            | yes      | X                |
| 464  | budget | fixed_price | 10       | 10               | -10  | 0            | *        | X                |
| 465  | budget | fixed_price | 10       | 10               | -10  | 0            | no       | X                |
| 466  | budget | fixed_price | 10       | 10               | -10  | 10           | yes      | X                |
| 467  | budget | fixed_price | 10       | 10               | -10  | 10           | *        | X                |
| 468  | budget | fixed_price | 10       | 10               | -10  | 10           | no       | X                |
| 469  | budget | fixed_price | 10       | 10               | 0    | -10          | yes      | X                |
| 470  | budget | fixed_price | 10       | 10               | 0    | -10          | *        | X                |
| 471  | budget | fixed_price | 10       | 10               | 0    | -10          | no       | X                |
| 472  | budget | fixed_price | 10       | 10               | 0    | 0            | yes      | 0                |
| 473  | budget | fixed_price | 10       | 10               | 0    | 0            | *        | X                |
| 474  | budget | fixed_price | 10       | 10               | 0    | 0            | no       | 0                |
| 475  | budget | fixed_price | 10       | 10               | 0    | 10           | yes      | 0                |
| 476  | budget | fixed_price | 10       | 10               | 0    | 10           | *        | X                |
| 477  | budget | fixed_price | 10       | 10               | 0    | 10           | no       | 0                |
| 478  | budget | fixed_price | 10       | 10               | 10   | -10          | yes      | X                |
| 479  | budget | fixed_price | 10       | 10               | 10   | -10          | *        | X                |
| 480  | budget | fixed_price | 10       | 10               | 10   | -10          | no       | X                |
| 481  | budget | fixed_price | 10       | 10               | 10   | 0            | yes      | 146.666666666667 |
| 482  | budget | fixed_price | 10       | 10               | 10   | 0            | *        | X                |
| 483  | budget | fixed_price | 10       | 10               | 10   | 0            | no       | 166.666666666667 |
| 484  | budget | fixed_price | 10       | 10               | 10   | 10           | yes      | 110              |
| 485  | budget | fixed_price | 10       | 10               | 10   | 10           | *        | X                |
| 486  | budget | fixed_price | 10       | 10               | 10   | 10           | no       | 125              |
| 487  | luxury | minute      | -10      | -10              | -10  | -10          | yes      | X                |
| 488  | luxury | minute      | -10      | -10              | -10  | -10          | *        | X                |
| 489  | luxury | minute      | -10      | -10              | -10  | -10          | no       | X                |
| 490  | luxury | minute      | -10      | -10              | -10  | 0            | yes      | X                |
| 491  | luxury | minute      | -10      | -10              | -10  | 0            | *        | X                |
| 492  | luxury | minute      | -10      | -10              | -10  | 0            | no       | X                |
| 493  | luxury | minute      | -10      | -10              | -10  | 10           | yes      | X                |
| 494  | luxury | minute      | -10      | -10              | -10  | 10           | *        | X                |
| 495  | luxury | minute      | -10      | -10              | -10  | 10           | no       | X                |
| 496  | luxury | minute      | -10      | -10              | 0    | -10          | yes      | X                |
| 497  | luxury | minute      | -10      | -10              | 0    | -10          | *        | X                |
| 498  | luxury | minute      | -10      | -10              | 0    | -10          | no       | X                |
| 499  | luxury | minute      | -10      | -10              | 0    | 0            | yes      | 0                |
| 500  | luxury | minute      | -10      | -10              | 0    | 0            | *        | X                |
| 501  | luxury | minute      | -10      | -10              | 0    | 0            | no       | 0                |
| 502  | luxury | minute      | -10      | -10              | 0    | 10           | yes      | 0                |
| 503  | luxury | minute      | -10      | -10              | 0    | 10           | *        | X                |
| 504  | luxury | minute      | -10      | -10              | 0    | 10           | no       | 0                |
| 505  | luxury | minute      | -10      | -10              | 10   | -10          | yes      | X                |
| 506  | luxury | minute      | -10      | -10              | 10   | -10          | *        | X                |
| 507  | luxury | minute      | -10      | -10              | 10   | -10          | no       | X                |
| 508  | luxury | minute      | -10      | -10              | 10   | 0            | yes      | 572              |
| 509  | luxury | minute      | -10      | -10              | 10   | 0            | *        | X                |
| 510  | luxury | minute      | -10      | -10              | 10   | 0            | no       | 650              |
| 511  | luxury | minute      | -10      | -10              | 10   | 10           | yes      | 572              |
| 512  | luxury | minute      | -10      | -10              | 10   | 10           | *        | X                |
| 513  | luxury | minute      | -10      | -10              | 10   | 10           | no       | 650              |
| 514  | luxury | minute      | -10      | 0                | -10  | -10          | yes      | X                |
| 515  | luxury | minute      | -10      | 0                | -10  | -10          | *        | X                |
| 516  | luxury | minute      | -10      | 0                | -10  | -10          | no       | X                |
| 517  | luxury | minute      | -10      | 0                | -10  | 0            | yes      | X                |
| 518  | luxury | minute      | -10      | 0                | -10  | 0            | *        | X                |
| 519  | luxury | minute      | -10      | 0                | -10  | 0            | no       | X                |
| 520  | luxury | minute      | -10      | 0                | -10  | 10           | yes      | X                |
| 521  | luxury | minute      | -10      | 0                | -10  | 10           | *        | X                |
| 522  | luxury | minute      | -10      | 0                | -10  | 10           | no       | X                |
| 523  | luxury | minute      | -10      | 0                | 0    | -10          | yes      | X                |
| 524  | luxury | minute      | -10      | 0                | 0    | -10          | *        | X                |
| 525  | luxury | minute      | -10      | 0                | 0    | -10          | no       | X                |
| 526  | luxury | minute      | -10      | 0                | 0    | 0            | yes      | 0                |
| 527  | luxury | minute      | -10      | 0                | 0    | 0            | *        | X                |
| 528  | luxury | minute      | -10      | 0                | 0    | 0            | no       | 0                |
| 529  | luxury | minute      | -10      | 0                | 0    | 10           | yes      | 0                |
| 530  | luxury | minute      | -10      | 0                | 0    | 10           | *        | X                |
| 531  | luxury | minute      | -10      | 0                | 0    | 10           | no       | 0                |
| 532  | luxury | minute      | -10      | 0                | 10   | -10          | yes      | X                |
| 533  | luxury | minute      | -10      | 0                | 10   | -10          | *        | X                |
| 534  | luxury | minute      | -10      | 0                | 10   | -10          | no       | X                |
| 535  | luxury | minute      | -10      | 0                | 10   | 0            | yes      | 572              |
| 536  | luxury | minute      | -10      | 0                | 10   | 0            | *        | X                |
| 537  | luxury | minute      | -10      | 0                | 10   | 0            | no       | 650              |
| 538  | luxury | minute      | -10      | 0                | 10   | 10           | yes      | 572              |
| 539  | luxury | minute      | -10      | 0                | 10   | 10           | *        | X                |
| 540  | luxury | minute      | -10      | 0                | 10   | 10           | no       | 650              |
| 541  | luxury | minute      | -10      | 10               | -10  | -10          | yes      | X                |
| 542  | luxury | minute      | -10      | 10               | -10  | -10          | *        | X                |
| 543  | luxury | minute      | -10      | 10               | -10  | -10          | no       | X                |
| 544  | luxury | minute      | -10      | 10               | -10  | 0            | yes      | X                |
| 545  | luxury | minute      | -10      | 10               | -10  | 0            | *        | X                |
| 546  | luxury | minute      | -10      | 10               | -10  | 0            | no       | X                |
| 547  | luxury | minute      | -10      | 10               | -10  | 10           | yes      | X                |
| 548  | luxury | minute      | -10      | 10               | -10  | 10           | *        | X                |
| 549  | luxury | minute      | -10      | 10               | -10  | 10           | no       | X                |
| 550  | luxury | minute      | -10      | 10               | 0    | -10          | yes      | X                |
| 551  | luxury | minute      | -10      | 10               | 0    | -10          | *        | X                |
| 552  | luxury | minute      | -10      | 10               | 0    | -10          | no       | X                |
| 553  | luxury | minute      | -10      | 10               | 0    | 0            | yes      | 0                |
| 554  | luxury | minute      | -10      | 10               | 0    | 0            | *        | X                |
| 555  | luxury | minute      | -10      | 10               | 0    | 0            | no       | 0                |
| 556  | luxury | minute      | -10      | 10               | 0    | 10           | yes      | 0                |
| 557  | luxury | minute      | -10      | 10               | 0    | 10           | *        | X                |
| 558  | luxury | minute      | -10      | 10               | 0    | 10           | no       | 0                |
| 559  | luxury | minute      | -10      | 10               | 10   | -10          | yes      | X                |
| 560  | luxury | minute      | -10      | 10               | 10   | -10          | *        | X                |
| 561  | luxury | minute      | -10      | 10               | 10   | -10          | no       | X                |
| 562  | luxury | minute      | -10      | 10               | 10   | 0            | yes      | 572              |
| 563  | luxury | minute      | -10      | 10               | 10   | 0            | *        | X                |
| 564  | luxury | minute      | -10      | 10               | 10   | 0            | no       | 650              |
| 565  | luxury | minute      | -10      | 10               | 10   | 10           | yes      | 572              |
| 566  | luxury | minute      | -10      | 10               | 10   | 10           | *        | X                |
| 567  | luxury | minute      | -10      | 10               | 10   | 10           | no       | 650              |
| 568  | luxury | minute      | 0        | -10              | -10  | -10          | yes      | X                |
| 569  | luxury | minute      | 0        | -10              | -10  | -10          | *        | X                |
| 570  | luxury | minute      | 0        | -10              | -10  | -10          | no       | X                |
| 571  | luxury | minute      | 0        | -10              | -10  | 0            | yes      | X                |
| 572  | luxury | minute      | 0        | -10              | -10  | 0            | *        | X                |
| 573  | luxury | minute      | 0        | -10              | -10  | 0            | no       | X                |
| 574  | luxury | minute      | 0        | -10              | -10  | 10           | yes      | X                |
| 575  | luxury | minute      | 0        | -10              | -10  | 10           | *        | X                |
| 576  | luxury | minute      | 0        | -10              | -10  | 10           | no       | X                |
| 577  | luxury | minute      | 0        | -10              | 0    | -10          | yes      | X                |
| 578  | luxury | minute      | 0        | -10              | 0    | -10          | *        | X                |
| 579  | luxury | minute      | 0        | -10              | 0    | -10          | no       | X                |
| 580  | luxury | minute      | 0        | -10              | 0    | 0            | yes      | 0                |
| 581  | luxury | minute      | 0        | -10              | 0    | 0            | *        | X                |
| 582  | luxury | minute      | 0        | -10              | 0    | 0            | no       | 0                |
| 583  | luxury | minute      | 0        | -10              | 0    | 10           | yes      | 0                |
| 584  | luxury | minute      | 0        | -10              | 0    | 10           | *        | X                |
| 585  | luxury | minute      | 0        | -10              | 0    | 10           | no       | 0                |
| 586  | luxury | minute      | 0        | -10              | 10   | -10          | yes      | X                |
| 587  | luxury | minute      | 0        | -10              | 10   | -10          | *        | X                |
| 588  | luxury | minute      | 0        | -10              | 10   | -10          | no       | X                |
| 589  | luxury | minute      | 0        | -10              | 10   | 0            | yes      | 572              |
| 590  | luxury | minute      | 0        | -10              | 10   | 0            | *        | X                |
| 591  | luxury | minute      | 0        | -10              | 10   | 0            | no       | 650              |
| 592  | luxury | minute      | 0        | -10              | 10   | 10           | yes      | 572              |
| 593  | luxury | minute      | 0        | -10              | 10   | 10           | *        | X                |
| 594  | luxury | minute      | 0        | -10              | 10   | 10           | no       | 650              |
| 595  | luxury | minute      | 0        | 0                | -10  | -10          | yes      | X                |
| 596  | luxury | minute      | 0        | 0                | -10  | -10          | *        | X                |
| 597  | luxury | minute      | 0        | 0                | -10  | -10          | no       | X                |
| 598  | luxury | minute      | 0        | 0                | -10  | 0            | yes      | X                |
| 599  | luxury | minute      | 0        | 0                | -10  | 0            | *        | X                |
| 600  | luxury | minute      | 0        | 0                | -10  | 0            | no       | X                |
| 601  | luxury | minute      | 0        | 0                | -10  | 10           | yes      | X                |
| 602  | luxury | minute      | 0        | 0                | -10  | 10           | *        | X                |
| 603  | luxury | minute      | 0        | 0                | -10  | 10           | no       | X                |
| 604  | luxury | minute      | 0        | 0                | 0    | -10          | yes      | X                |
| 605  | luxury | minute      | 0        | 0                | 0    | -10          | *        | X                |
| 606  | luxury | minute      | 0        | 0                | 0    | -10          | no       | X                |
| 607  | luxury | minute      | 0        | 0                | 0    | 0            | yes      | 0                |
| 608  | luxury | minute      | 0        | 0                | 0    | 0            | *        | X                |
| 609  | luxury | minute      | 0        | 0                | 0    | 0            | no       | 0                |
| 610  | luxury | minute      | 0        | 0                | 0    | 10           | yes      | 0                |
| 611  | luxury | minute      | 0        | 0                | 0    | 10           | *        | X                |
| 612  | luxury | minute      | 0        | 0                | 0    | 10           | no       | 0                |
| 613  | luxury | minute      | 0        | 0                | 10   | -10          | yes      | X                |
| 614  | luxury | minute      | 0        | 0                | 10   | -10          | *        | X                |
| 615  | luxury | minute      | 0        | 0                | 10   | -10          | no       | X                |
| 616  | luxury | minute      | 0        | 0                | 10   | 0            | yes      | 572              |
| 617  | luxury | minute      | 0        | 0                | 10   | 0            | *        | X                |
| 618  | luxury | minute      | 0        | 0                | 10   | 0            | no       | 650              |
| 619  | luxury | minute      | 0        | 0                | 10   | 10           | yes      | 572              |
| 620  | luxury | minute      | 0        | 0                | 10   | 10           | *        | X                |
| 621  | luxury | minute      | 0        | 0                | 10   | 10           | no       | 650              |
| 622  | luxury | minute      | 0        | 10               | -10  | -10          | yes      | X                |
| 623  | luxury | minute      | 0        | 10               | -10  | -10          | *        | X                |
| 624  | luxury | minute      | 0        | 10               | -10  | -10          | no       | X                |
| 625  | luxury | minute      | 0        | 10               | -10  | 0            | yes      | X                |
| 626  | luxury | minute      | 0        | 10               | -10  | 0            | *        | X                |
| 627  | luxury | minute      | 0        | 10               | -10  | 0            | no       | X                |
| 628  | luxury | minute      | 0        | 10               | -10  | 10           | yes      | X                |
| 629  | luxury | minute      | 0        | 10               | -10  | 10           | *        | X                |
| 630  | luxury | minute      | 0        | 10               | -10  | 10           | no       | X                |
| 631  | luxury | minute      | 0        | 10               | 0    | -10          | yes      | X                |
| 632  | luxury | minute      | 0        | 10               | 0    | -10          | *        | X                |
| 633  | luxury | minute      | 0        | 10               | 0    | -10          | no       | X                |
| 634  | luxury | minute      | 0        | 10               | 0    | 0            | yes      | 0                |
| 635  | luxury | minute      | 0        | 10               | 0    | 0            | *        | X                |
| 636  | luxury | minute      | 0        | 10               | 0    | 0            | no       | 0                |
| 637  | luxury | minute      | 0        | 10               | 0    | 10           | yes      | 0                |
| 638  | luxury | minute      | 0        | 10               | 0    | 10           | *        | X                |
| 639  | luxury | minute      | 0        | 10               | 0    | 10           | no       | 0                |
| 640  | luxury | minute      | 0        | 10               | 10   | -10          | yes      | X                |
| 641  | luxury | minute      | 0        | 10               | 10   | -10          | *        | X                |
| 642  | luxury | minute      | 0        | 10               | 10   | -10          | no       | X                |
| 643  | luxury | minute      | 0        | 10               | 10   | 0            | yes      | 572              |
| 644  | luxury | minute      | 0        | 10               | 10   | 0            | *        | X                |
| 645  | luxury | minute      | 0        | 10               | 10   | 0            | no       | 650              |
| 646  | luxury | minute      | 0        | 10               | 10   | 10           | yes      | 572              |
| 647  | luxury | minute      | 0        | 10               | 10   | 10           | *        | X                |
| 648  | luxury | minute      | 0        | 10               | 10   | 10           | no       | 650              |
| 649  | luxury | minute      | 10       | -10              | -10  | -10          | yes      | X                |
| 650  | luxury | minute      | 10       | -10              | -10  | -10          | *        | X                |
| 651  | luxury | minute      | 10       | -10              | -10  | -10          | no       | X                |
| 652  | luxury | minute      | 10       | -10              | -10  | 0            | yes      | X                |
| 653  | luxury | minute      | 10       | -10              | -10  | 0            | *        | X                |
| 654  | luxury | minute      | 10       | -10              | -10  | 0            | no       | X                |
| 655  | luxury | minute      | 10       | -10              | -10  | 10           | yes      | X                |
| 656  | luxury | minute      | 10       | -10              | -10  | 10           | *        | X                |
| 657  | luxury | minute      | 10       | -10              | -10  | 10           | no       | X                |
| 658  | luxury | minute      | 10       | -10              | 0    | -10          | yes      | X                |
| 659  | luxury | minute      | 10       | -10              | 0    | -10          | *        | X                |
| 660  | luxury | minute      | 10       | -10              | 0    | -10          | no       | X                |
| 661  | luxury | minute      | 10       | -10              | 0    | 0            | yes      | 0                |
| 662  | luxury | minute      | 10       | -10              | 0    | 0            | *        | X                |
| 663  | luxury | minute      | 10       | -10              | 0    | 0            | no       | 0                |
| 664  | luxury | minute      | 10       | -10              | 0    | 10           | yes      | 0                |
| 665  | luxury | minute      | 10       | -10              | 0    | 10           | *        | X                |
| 666  | luxury | minute      | 10       | -10              | 0    | 10           | no       | 0                |
| 667  | luxury | minute      | 10       | -10              | 10   | -10          | yes      | X                |
| 668  | luxury | minute      | 10       | -10              | 10   | -10          | *        | X                |
| 669  | luxury | minute      | 10       | -10              | 10   | -10          | no       | X                |
| 670  | luxury | minute      | 10       | -10              | 10   | 0            | yes      | 572              |
| 671  | luxury | minute      | 10       | -10              | 10   | 0            | *        | X                |
| 672  | luxury | minute      | 10       | -10              | 10   | 0            | no       | 650              |
| 673  | luxury | minute      | 10       | -10              | 10   | 10           | yes      | 572              |
| 674  | luxury | minute      | 10       | -10              | 10   | 10           | *        | X                |
| 675  | luxury | minute      | 10       | -10              | 10   | 10           | no       | 650              |
| 676  | luxury | minute      | 10       | 0                | -10  | -10          | yes      | X                |
| 677  | luxury | minute      | 10       | 0                | -10  | -10          | *        | X                |
| 678  | luxury | minute      | 10       | 0                | -10  | -10          | no       | X                |
| 679  | luxury | minute      | 10       | 0                | -10  | 0            | yes      | X                |
| 680  | luxury | minute      | 10       | 0                | -10  | 0            | *        | X                |
| 681  | luxury | minute      | 10       | 0                | -10  | 0            | no       | X                |
| 682  | luxury | minute      | 10       | 0                | -10  | 10           | yes      | X                |
| 683  | luxury | minute      | 10       | 0                | -10  | 10           | *        | X                |
| 684  | luxury | minute      | 10       | 0                | -10  | 10           | no       | X                |
| 685  | luxury | minute      | 10       | 0                | 0    | -10          | yes      | X                |
| 686  | luxury | minute      | 10       | 0                | 0    | -10          | *        | X                |
| 687  | luxury | minute      | 10       | 0                | 0    | -10          | no       | X                |
| 688  | luxury | minute      | 10       | 0                | 0    | 0            | yes      | 0                |
| 689  | luxury | minute      | 10       | 0                | 0    | 0            | *        | X                |
| 690  | luxury | minute      | 10       | 0                | 0    | 0            | no       | 0                |
| 691  | luxury | minute      | 10       | 0                | 0    | 10           | yes      | 0                |
| 692  | luxury | minute      | 10       | 0                | 0    | 10           | *        | X                |
| 693  | luxury | minute      | 10       | 0                | 0    | 10           | no       | 0                |
| 694  | luxury | minute      | 10       | 0                | 10   | -10          | yes      | X                |
| 695  | luxury | minute      | 10       | 0                | 10   | -10          | *        | X                |
| 696  | luxury | minute      | 10       | 0                | 10   | -10          | no       | X                |
| 697  | luxury | minute      | 10       | 0                | 10   | 0            | yes      | 572              |
| 698  | luxury | minute      | 10       | 0                | 10   | 0            | *        | X                |
| 699  | luxury | minute      | 10       | 0                | 10   | 0            | no       | 650              |
| 700  | luxury | minute      | 10       | 0                | 10   | 10           | yes      | 572              |
| 701  | luxury | minute      | 10       | 0                | 10   | 10           | *        | X                |
| 702  | luxury | minute      | 10       | 0                | 10   | 10           | no       | 650              |
| 703  | luxury | minute      | 10       | 10               | -10  | -10          | yes      | X                |
| 704  | luxury | minute      | 10       | 10               | -10  | -10          | *        | X                |
| 705  | luxury | minute      | 10       | 10               | -10  | -10          | no       | X                |
| 706  | luxury | minute      | 10       | 10               | -10  | 0            | yes      | X                |
| 707  | luxury | minute      | 10       | 10               | -10  | 0            | *        | X                |
| 708  | luxury | minute      | 10       | 10               | -10  | 0            | no       | X                |
| 709  | luxury | minute      | 10       | 10               | -10  | 10           | yes      | X                |
| 710  | luxury | minute      | 10       | 10               | -10  | 10           | *        | X                |
| 711  | luxury | minute      | 10       | 10               | -10  | 10           | no       | X                |
| 712  | luxury | minute      | 10       | 10               | 0    | -10          | yes      | X                |
| 713  | luxury | minute      | 10       | 10               | 0    | -10          | *        | X                |
| 714  | luxury | minute      | 10       | 10               | 0    | -10          | no       | X                |
| 715  | luxury | minute      | 10       | 10               | 0    | 0            | yes      | 0                |
| 716  | luxury | minute      | 10       | 10               | 0    | 0            | *        | X                |
| 717  | luxury | minute      | 10       | 10               | 0    | 0            | no       | 0                |
| 718  | luxury | minute      | 10       | 10               | 0    | 10           | yes      | 0                |
| 719  | luxury | minute      | 10       | 10               | 0    | 10           | *        | X                |
| 720  | luxury | minute      | 10       | 10               | 0    | 10           | no       | 0                |
| 721  | luxury | minute      | 10       | 10               | 10   | -10          | yes      | X                |
| 722  | luxury | minute      | 10       | 10               | 10   | -10          | *        | X                |
| 723  | luxury | minute      | 10       | 10               | 10   | -10          | no       | X                |
| 724  | luxury | minute      | 10       | 10               | 10   | 0            | yes      | 572              |
| 725  | luxury | minute      | 10       | 10               | 10   | 0            | *        | X                |
| 726  | luxury | minute      | 10       | 10               | 10   | 0            | no       | 650              |
| 727  | luxury | minute      | 10       | 10               | 10   | 10           | yes      | 572              |
| 728  | luxury | minute      | 10       | 10               | 10   | 10           | *        | X                |
| 729  | luxury | minute      | 10       | 10               | 10   | 10           | no       | 650              |
| 730  | luxury | fixed_price | -10      | -10              | -10  | -10          | yes      | X                |
| 731  | luxury | fixed_price | -10      | -10              | -10  | -10          | *        | X                |
| 732  | luxury | fixed_price | -10      | -10              | -10  | -10          | no       | X                |
| 733  | luxury | fixed_price | -10      | -10              | -10  | 0            | yes      | X                |
| 734  | luxury | fixed_price | -10      | -10              | -10  | 0            | *        | X                |
| 735  | luxury | fixed_price | -10      | -10              | -10  | 0            | no       | X                |
| 736  | luxury | fixed_price | -10      | -10              | -10  | 10           | yes      | X                |
| 737  | luxury | fixed_price | -10      | -10              | -10  | 10           | *        | X                |
| 738  | luxury | fixed_price | -10      | -10              | -10  | 10           | no       | X                |
| 739  | luxury | fixed_price | -10      | -10              | 0    | -10          | yes      | X                |
| 740  | luxury | fixed_price | -10      | -10              | 0    | -10          | *        | X                |
| 741  | luxury | fixed_price | -10      | -10              | 0    | -10          | no       | X                |
| 742  | luxury | fixed_price | -10      | -10              | 0    | 0            | yes      | X                |
| 743  | luxury | fixed_price | -10      | -10              | 0    | 0            | *        | X                |
| 744  | luxury | fixed_price | -10      | -10              | 0    | 0            | no       | X                |
| 745  | luxury | fixed_price | -10      | -10              | 0    | 10           | yes      | X                |
| 746  | luxury | fixed_price | -10      | -10              | 0    | 10           | *        | X                |
| 747  | luxury | fixed_price | -10      | -10              | 0    | 10           | no       | X                |
| 748  | luxury | fixed_price | -10      | -10              | 10   | -10          | yes      | X                |
| 749  | luxury | fixed_price | -10      | -10              | 10   | -10          | *        | X                |
| 750  | luxury | fixed_price | -10      | -10              | 10   | -10          | no       | X                |
| 751  | luxury | fixed_price | -10      | -10              | 10   | 0            | yes      | X                |
| 752  | luxury | fixed_price | -10      | -10              | 10   | 0            | *        | X                |
| 753  | luxury | fixed_price | -10      | -10              | 10   | 0            | no       | X                |
| 754  | luxury | fixed_price | -10      | -10              | 10   | 10           | yes      | X                |
| 755  | luxury | fixed_price | -10      | -10              | 10   | 10           | *        | X                |
| 756  | luxury | fixed_price | -10      | -10              | 10   | 10           | no       | X                |
| 757  | luxury | fixed_price | -10      | 0                | -10  | -10          | yes      | X                |
| 758  | luxury | fixed_price | -10      | 0                | -10  | -10          | *        | X                |
| 759  | luxury | fixed_price | -10      | 0                | -10  | -10          | no       | X                |
| 760  | luxury | fixed_price | -10      | 0                | -10  | 0            | yes      | X                |
| 761  | luxury | fixed_price | -10      | 0                | -10  | 0            | *        | X                |
| 762  | luxury | fixed_price | -10      | 0                | -10  | 0            | no       | X                |
| 763  | luxury | fixed_price | -10      | 0                | -10  | 10           | yes      | X                |
| 764  | luxury | fixed_price | -10      | 0                | -10  | 10           | *        | X                |
| 765  | luxury | fixed_price | -10      | 0                | -10  | 10           | no       | X                |
| 766  | luxury | fixed_price | -10      | 0                | 0    | -10          | yes      | X                |
| 767  | luxury | fixed_price | -10      | 0                | 0    | -10          | *        | X                |
| 768  | luxury | fixed_price | -10      | 0                | 0    | -10          | no       | X                |
| 769  | luxury | fixed_price | -10      | 0                | 0    | 0            | yes      | X                |
| 770  | luxury | fixed_price | -10      | 0                | 0    | 0            | *        | X                |
| 771  | luxury | fixed_price | -10      | 0                | 0    | 0            | no       | X                |
| 772  | luxury | fixed_price | -10      | 0                | 0    | 10           | yes      | X                |
| 773  | luxury | fixed_price | -10      | 0                | 0    | 10           | *        | X                |
| 774  | luxury | fixed_price | -10      | 0                | 0    | 10           | no       | X                |
| 775  | luxury | fixed_price | -10      | 0                | 10   | -10          | yes      | X                |
| 776  | luxury | fixed_price | -10      | 0                | 10   | -10          | *        | X                |
| 777  | luxury | fixed_price | -10      | 0                | 10   | -10          | no       | X                |
| 778  | luxury | fixed_price | -10      | 0                | 10   | 0            | yes      | X                |
| 779  | luxury | fixed_price | -10      | 0                | 10   | 0            | *        | X                |
| 780  | luxury | fixed_price | -10      | 0                | 10   | 0            | no       | X                |
| 781  | luxury | fixed_price | -10      | 0                | 10   | 10           | yes      | X                |
| 782  | luxury | fixed_price | -10      | 0                | 10   | 10           | *        | X                |
| 783  | luxury | fixed_price | -10      | 0                | 10   | 10           | no       | X                |
| 784  | luxury | fixed_price | -10      | 10               | -10  | -10          | yes      | X                |
| 785  | luxury | fixed_price | -10      | 10               | -10  | -10          | *        | X                |
| 786  | luxury | fixed_price | -10      | 10               | -10  | -10          | no       | X                |
| 787  | luxury | fixed_price | -10      | 10               | -10  | 0            | yes      | X                |
| 788  | luxury | fixed_price | -10      | 10               | -10  | 0            | *        | X                |
| 789  | luxury | fixed_price | -10      | 10               | -10  | 0            | no       | X                |
| 790  | luxury | fixed_price | -10      | 10               | -10  | 10           | yes      | X                |
| 791  | luxury | fixed_price | -10      | 10               | -10  | 10           | *        | X                |
| 792  | luxury | fixed_price | -10      | 10               | -10  | 10           | no       | X                |
| 793  | luxury | fixed_price | -10      | 10               | 0    | -10          | yes      | X                |
| 794  | luxury | fixed_price | -10      | 10               | 0    | -10          | *        | X                |
| 795  | luxury | fixed_price | -10      | 10               | 0    | -10          | no       | X                |
| 796  | luxury | fixed_price | -10      | 10               | 0    | 0            | yes      | X                |
| 797  | luxury | fixed_price | -10      | 10               | 0    | 0            | *        | X                |
| 798  | luxury | fixed_price | -10      | 10               | 0    | 0            | no       | X                |
| 799  | luxury | fixed_price | -10      | 10               | 0    | 10           | yes      | X                |
| 800  | luxury | fixed_price | -10      | 10               | 0    | 10           | *        | X                |
| 801  | luxury | fixed_price | -10      | 10               | 0    | 10           | no       | X                |
| 802  | luxury | fixed_price | -10      | 10               | 10   | -10          | yes      | X                |
| 803  | luxury | fixed_price | -10      | 10               | 10   | -10          | *        | X                |
| 804  | luxury | fixed_price | -10      | 10               | 10   | -10          | no       | X                |
| 805  | luxury | fixed_price | -10      | 10               | 10   | 0            | yes      | X                |
| 806  | luxury | fixed_price | -10      | 10               | 10   | 0            | *        | X                |
| 807  | luxury | fixed_price | -10      | 10               | 10   | 0            | no       | X                |
| 808  | luxury | fixed_price | -10      | 10               | 10   | 10           | yes      | X                |
| 809  | luxury | fixed_price | -10      | 10               | 10   | 10           | *        | X                |
| 810  | luxury | fixed_price | -10      | 10               | 10   | 10           | no       | X                |
| 811  | luxury | fixed_price | 0        | -10              | -10  | -10          | yes      | X                |
| 812  | luxury | fixed_price | 0        | -10              | -10  | -10          | *        | X                |
| 813  | luxury | fixed_price | 0        | -10              | -10  | -10          | no       | X                |
| 814  | luxury | fixed_price | 0        | -10              | -10  | 0            | yes      | X                |
| 815  | luxury | fixed_price | 0        | -10              | -10  | 0            | *        | X                |
| 816  | luxury | fixed_price | 0        | -10              | -10  | 0            | no       | X                |
| 817  | luxury | fixed_price | 0        | -10              | -10  | 10           | yes      | X                |
| 818  | luxury | fixed_price | 0        | -10              | -10  | 10           | *        | X                |
| 819  | luxury | fixed_price | 0        | -10              | -10  | 10           | no       | X                |
| 820  | luxury | fixed_price | 0        | -10              | 0    | -10          | yes      | X                |
| 821  | luxury | fixed_price | 0        | -10              | 0    | -10          | *        | X                |
| 822  | luxury | fixed_price | 0        | -10              | 0    | -10          | no       | X                |
| 823  | luxury | fixed_price | 0        | -10              | 0    | 0            | yes      | X                |
| 824  | luxury | fixed_price | 0        | -10              | 0    | 0            | *        | X                |
| 825  | luxury | fixed_price | 0        | -10              | 0    | 0            | no       | X                |
| 826  | luxury | fixed_price | 0        | -10              | 0    | 10           | yes      | X                |
| 827  | luxury | fixed_price | 0        | -10              | 0    | 10           | *        | X                |
| 828  | luxury | fixed_price | 0        | -10              | 0    | 10           | no       | X                |
| 829  | luxury | fixed_price | 0        | -10              | 10   | -10          | yes      | X                |
| 830  | luxury | fixed_price | 0        | -10              | 10   | -10          | *        | X                |
| 831  | luxury | fixed_price | 0        | -10              | 10   | -10          | no       | X                |
| 832  | luxury | fixed_price | 0        | -10              | 10   | 0            | yes      | X                |
| 833  | luxury | fixed_price | 0        | -10              | 10   | 0            | *        | X                |
| 834  | luxury | fixed_price | 0        | -10              | 10   | 0            | no       | X                |
| 835  | luxury | fixed_price | 0        | -10              | 10   | 10           | yes      | X                |
| 836  | luxury | fixed_price | 0        | -10              | 10   | 10           | *        | X                |
| 837  | luxury | fixed_price | 0        | -10              | 10   | 10           | no       | X                |
| 838  | luxury | fixed_price | 0        | 0                | -10  | -10          | yes      | X                |
| 839  | luxury | fixed_price | 0        | 0                | -10  | -10          | *        | X                |
| 840  | luxury | fixed_price | 0        | 0                | -10  | -10          | no       | X                |
| 841  | luxury | fixed_price | 0        | 0                | -10  | 0            | yes      | X                |
| 842  | luxury | fixed_price | 0        | 0                | -10  | 0            | *        | X                |
| 843  | luxury | fixed_price | 0        | 0                | -10  | 0            | no       | X                |
| 844  | luxury | fixed_price | 0        | 0                | -10  | 10           | yes      | X                |
| 845  | luxury | fixed_price | 0        | 0                | -10  | 10           | *        | X                |
| 846  | luxury | fixed_price | 0        | 0                | -10  | 10           | no       | X                |
| 847  | luxury | fixed_price | 0        | 0                | 0    | -10          | yes      | X                |
| 848  | luxury | fixed_price | 0        | 0                | 0    | -10          | *        | X                |
| 849  | luxury | fixed_price | 0        | 0                | 0    | -10          | no       | X                |
| 850  | luxury | fixed_price | 0        | 0                | 0    | 0            | yes      | X                |
| 851  | luxury | fixed_price | 0        | 0                | 0    | 0            | *        | X                |
| 852  | luxury | fixed_price | 0        | 0                | 0    | 0            | no       | X                |
| 853  | luxury | fixed_price | 0        | 0                | 0    | 10           | yes      | X                |
| 854  | luxury | fixed_price | 0        | 0                | 0    | 10           | *        | X                |
| 855  | luxury | fixed_price | 0        | 0                | 0    | 10           | no       | X                |
| 856  | luxury | fixed_price | 0        | 0                | 10   | -10          | yes      | X                |
| 857  | luxury | fixed_price | 0        | 0                | 10   | -10          | *        | X                |
| 858  | luxury | fixed_price | 0        | 0                | 10   | -10          | no       | X                |
| 859  | luxury | fixed_price | 0        | 0                | 10   | 0            | yes      | X                |
| 860  | luxury | fixed_price | 0        | 0                | 10   | 0            | *        | X                |
| 861  | luxury | fixed_price | 0        | 0                | 10   | 0            | no       | X                |
| 862  | luxury | fixed_price | 0        | 0                | 10   | 10           | yes      | X                |
| 863  | luxury | fixed_price | 0        | 0                | 10   | 10           | *        | X                |
| 864  | luxury | fixed_price | 0        | 0                | 10   | 10           | no       | X                |
| 865  | luxury | fixed_price | 0        | 10               | -10  | -10          | yes      | X                |
| 866  | luxury | fixed_price | 0        | 10               | -10  | -10          | *        | X                |
| 867  | luxury | fixed_price | 0        | 10               | -10  | -10          | no       | X                |
| 868  | luxury | fixed_price | 0        | 10               | -10  | 0            | yes      | X                |
| 869  | luxury | fixed_price | 0        | 10               | -10  | 0            | *        | X                |
| 870  | luxury | fixed_price | 0        | 10               | -10  | 0            | no       | X                |
| 871  | luxury | fixed_price | 0        | 10               | -10  | 10           | yes      | X                |
| 872  | luxury | fixed_price | 0        | 10               | -10  | 10           | *        | X                |
| 873  | luxury | fixed_price | 0        | 10               | -10  | 10           | no       | X                |
| 874  | luxury | fixed_price | 0        | 10               | 0    | -10          | yes      | X                |
| 875  | luxury | fixed_price | 0        | 10               | 0    | -10          | *        | X                |
| 876  | luxury | fixed_price | 0        | 10               | 0    | -10          | no       | X                |
| 877  | luxury | fixed_price | 0        | 10               | 0    | 0            | yes      | X                |
| 878  | luxury | fixed_price | 0        | 10               | 0    | 0            | *        | X                |
| 879  | luxury | fixed_price | 0        | 10               | 0    | 0            | no       | X                |
| 880  | luxury | fixed_price | 0        | 10               | 0    | 10           | yes      | X                |
| 881  | luxury | fixed_price | 0        | 10               | 0    | 10           | *        | X                |
| 882  | luxury | fixed_price | 0        | 10               | 0    | 10           | no       | X                |
| 883  | luxury | fixed_price | 0        | 10               | 10   | -10          | yes      | X                |
| 884  | luxury | fixed_price | 0        | 10               | 10   | -10          | *        | X                |
| 885  | luxury | fixed_price | 0        | 10               | 10   | -10          | no       | X                |
| 886  | luxury | fixed_price | 0        | 10               | 10   | 0            | yes      | X                |
| 887  | luxury | fixed_price | 0        | 10               | 10   | 0            | *        | X                |
| 888  | luxury | fixed_price | 0        | 10               | 10   | 0            | no       | X                |
| 889  | luxury | fixed_price | 0        | 10               | 10   | 10           | yes      | X                |
| 890  | luxury | fixed_price | 0        | 10               | 10   | 10           | *        | X                |
| 891  | luxury | fixed_price | 0        | 10               | 10   | 10           | no       | X                |
| 892  | luxury | fixed_price | 10       | -10              | -10  | -10          | yes      | X                |
| 893  | luxury | fixed_price | 10       | -10              | -10  | -10          | *        | X                |
| 894  | luxury | fixed_price | 10       | -10              | -10  | -10          | no       | X                |
| 895  | luxury | fixed_price | 10       | -10              | -10  | 0            | yes      | X                |
| 896  | luxury | fixed_price | 10       | -10              | -10  | 0            | *        | X                |
| 897  | luxury | fixed_price | 10       | -10              | -10  | 0            | no       | X                |
| 898  | luxury | fixed_price | 10       | -10              | -10  | 10           | yes      | X                |
| 899  | luxury | fixed_price | 10       | -10              | -10  | 10           | *        | X                |
| 900  | luxury | fixed_price | 10       | -10              | -10  | 10           | no       | X                |
| 901  | luxury | fixed_price | 10       | -10              | 0    | -10          | yes      | X                |
| 902  | luxury | fixed_price | 10       | -10              | 0    | -10          | *        | X                |
| 903  | luxury | fixed_price | 10       | -10              | 0    | -10          | no       | X                |
| 904  | luxury | fixed_price | 10       | -10              | 0    | 0            | yes      | X                |
| 905  | luxury | fixed_price | 10       | -10              | 0    | 0            | *        | X                |
| 906  | luxury | fixed_price | 10       | -10              | 0    | 0            | no       | X                |
| 907  | luxury | fixed_price | 10       | -10              | 0    | 10           | yes      | X                |
| 908  | luxury | fixed_price | 10       | -10              | 0    | 10           | *        | X                |
| 909  | luxury | fixed_price | 10       | -10              | 0    | 10           | no       | X                |
| 910  | luxury | fixed_price | 10       | -10              | 10   | -10          | yes      | X                |
| 911  | luxury | fixed_price | 10       | -10              | 10   | -10          | *        | X                |
| 912  | luxury | fixed_price | 10       | -10              | 10   | -10          | no       | X                |
| 913  | luxury | fixed_price | 10       | -10              | 10   | 0            | yes      | X                |
| 914  | luxury | fixed_price | 10       | -10              | 10   | 0            | *        | X                |
| 915  | luxury | fixed_price | 10       | -10              | 10   | 0            | no       | X                |
| 916  | luxury | fixed_price | 10       | -10              | 10   | 10           | yes      | X                |
| 917  | luxury | fixed_price | 10       | -10              | 10   | 10           | *        | X                |
| 918  | luxury | fixed_price | 10       | -10              | 10   | 10           | no       | X                |
| 919  | luxury | fixed_price | 10       | 0                | -10  | -10          | yes      | X                |
| 920  | luxury | fixed_price | 10       | 0                | -10  | -10          | *        | X                |
| 921  | luxury | fixed_price | 10       | 0                | -10  | -10          | no       | X                |
| 922  | luxury | fixed_price | 10       | 0                | -10  | 0            | yes      | X                |
| 923  | luxury | fixed_price | 10       | 0                | -10  | 0            | *        | X                |
| 924  | luxury | fixed_price | 10       | 0                | -10  | 0            | no       | X                |
| 925  | luxury | fixed_price | 10       | 0                | -10  | 10           | yes      | X                |
| 926  | luxury | fixed_price | 10       | 0                | -10  | 10           | *        | X                |
| 927  | luxury | fixed_price | 10       | 0                | -10  | 10           | no       | X                |
| 928  | luxury | fixed_price | 10       | 0                | 0    | -10          | yes      | X                |
| 929  | luxury | fixed_price | 10       | 0                | 0    | -10          | *        | X                |
| 930  | luxury | fixed_price | 10       | 0                | 0    | -10          | no       | X                |
| 931  | luxury | fixed_price | 10       | 0                | 0    | 0            | yes      | X                |
| 932  | luxury | fixed_price | 10       | 0                | 0    | 0            | *        | X                |
| 933  | luxury | fixed_price | 10       | 0                | 0    | 0            | no       | X                |
| 934  | luxury | fixed_price | 10       | 0                | 0    | 10           | yes      | X                |
| 935  | luxury | fixed_price | 10       | 0                | 0    | 10           | *        | X                |
| 936  | luxury | fixed_price | 10       | 0                | 0    | 10           | no       | X                |
| 937  | luxury | fixed_price | 10       | 0                | 10   | -10          | yes      | X                |
| 938  | luxury | fixed_price | 10       | 0                | 10   | -10          | *        | X                |
| 939  | luxury | fixed_price | 10       | 0                | 10   | -10          | no       | X                |
| 940  | luxury | fixed_price | 10       | 0                | 10   | 0            | yes      | X                |
| 941  | luxury | fixed_price | 10       | 0                | 10   | 0            | *        | X                |
| 942  | luxury | fixed_price | 10       | 0                | 10   | 0            | no       | X                |
| 943  | luxury | fixed_price | 10       | 0                | 10   | 10           | yes      | X                |
| 944  | luxury | fixed_price | 10       | 0                | 10   | 10           | *        | X                |
| 945  | luxury | fixed_price | 10       | 0                | 10   | 10           | no       | X                |
| 946  | luxury | fixed_price | 10       | 10               | -10  | -10          | yes      | X                |
| 947  | luxury | fixed_price | 10       | 10               | -10  | -10          | *        | X                |
| 948  | luxury | fixed_price | 10       | 10               | -10  | -10          | no       | X                |
| 949  | luxury | fixed_price | 10       | 10               | -10  | 0            | yes      | X                |
| 950  | luxury | fixed_price | 10       | 10               | -10  | 0            | *        | X                |
| 951  | luxury | fixed_price | 10       | 10               | -10  | 0            | no       | X                |
| 952  | luxury | fixed_price | 10       | 10               | -10  | 10           | yes      | X                |
| 953  | luxury | fixed_price | 10       | 10               | -10  | 10           | *        | X                |
| 954  | luxury | fixed_price | 10       | 10               | -10  | 10           | no       | X                |
| 955  | luxury | fixed_price | 10       | 10               | 0    | -10          | yes      | X                |
| 956  | luxury | fixed_price | 10       | 10               | 0    | -10          | *        | X                |
| 957  | luxury | fixed_price | 10       | 10               | 0    | -10          | no       | X                |
| 958  | luxury | fixed_price | 10       | 10               | 0    | 0            | yes      | X                |
| 959  | luxury | fixed_price | 10       | 10               | 0    | 0            | *        | X                |
| 960  | luxury | fixed_price | 10       | 10               | 0    | 0            | no       | X                |
| 961  | luxury | fixed_price | 10       | 10               | 0    | 10           | yes      | X                |
| 962  | luxury | fixed_price | 10       | 10               | 0    | 10           | *        | X                |
| 963  | luxury | fixed_price | 10       | 10               | 0    | 10           | no       | X                |
| 964  | luxury | fixed_price | 10       | 10               | 10   | -10          | yes      | X                |
| 965  | luxury | fixed_price | 10       | 10               | 10   | -10          | *        | X                |
| 966  | luxury | fixed_price | 10       | 10               | 10   | -10          | no       | X                |
| 967  | luxury | fixed_price | 10       | 10               | 10   | 0            | yes      | X                |
| 968  | luxury | fixed_price | 10       | 10               | 10   | 0            | *        | X                |
| 969  | luxury | fixed_price | 10       | 10               | 10   | 0            | no       | X                |
| 970  | luxury | fixed_price | 10       | 10               | 10   | 10           | yes      | X                |
| 971  | luxury | fixed_price | 10       | 10               | 10   | 10           | *        | X                |
| 972  | luxury | fixed_price | 10       | 10               | 10   | 10           | no       | X                |
